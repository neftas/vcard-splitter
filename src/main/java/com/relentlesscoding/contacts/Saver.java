package com.relentlesscoding.contacts;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Handles the saving of vCard contacts to storage.
 */
class Saver implements Runnable {
    private static final String EXTENSION = ".vcf";
    private static final String UTF_8 = "UTF-8";

    private File baseDir;
    private ArrayBlockingQueue<String> queue;

    /**
     * Constructs a new object of type com.relentlesscoding.contacts.Saver.
     * @param baseDir The directory taken to be the base of other paths. Cannot be {@code null}.
     */
    Saver(final File baseDir, final ArrayBlockingQueue<String> queue) {
        if (baseDir == null) {
            throw new NullPointerException("'baseDir' is null");
        }
        if (queue == null) {
            throw new NullPointerException("'queue' is null");
        }

        this.baseDir = baseDir;
        this.queue = queue;
    }

    /**
     * Saves the vCard data in {@code contact} to file with a randomly generated file name,
     * consisting of the {@code baseDir} and a UUID plus {@code EXTENSION}.
     * @param contact The vCard contact. Cannot be {@code null} or the empty {@link String}.
     */
    private void saveToFile(final String contact) {
        assert contact != null : "'contact' is null";
        assert !contact.isEmpty() : "'contact' is the empty string";

        final File dest = new File(baseDir, createUUID() + EXTENSION);
        try (PrintWriter writer = new PrintWriter(dest, UTF_8)) {
            writer.println(contact);
        } catch (IOException e) {
            System.err.format("Could not write to file '%s'.", dest);
            System.exit(5);
        }
    }

    /**
     * Creates a random UUID based on Java's {@link UUID#randomUUID} with all dashes {@code -} removed.
     * @return A random UUID.
     */
    static String createUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        synchronized (this) {
            try {
                String contact;
                while ((contact = this.queue.poll(1, TimeUnit.SECONDS)) != null) {
                    saveToFile(contact);
                }
            } catch (InterruptedException e) {
                System.exit(1);
            }
        }
    }
}
