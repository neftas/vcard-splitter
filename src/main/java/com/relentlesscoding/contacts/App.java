package com.relentlesscoding.contacts;

import org.apache.commons.cli.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.ArrayBlockingQueue;

final class App {

    private static Options cliOptions = new Options();
    private CommandLine commandLine;
    private File source;
    private File dest;

    private static final String SOURCE = "source";
    private static final String DEST = "dest";
    private static final String HELP = "help";

    static {
        cliOptions.addOption("f", SOURCE, true, "The vCards source file");
        cliOptions.addOption("t", DEST, true, "The destination directory");
        cliOptions.addOption("h", HELP, false, "Print this help text and exit");
    }

    public static void main(String[] args) throws Exception {
        new App().start(args);
    }

    /**
     * Sets up the application.
     * @param args The command-line arguments.
     */
    private void start(String[] args) {
        assert args != null : "'args' is null";

        parseCommandLineParameters(args);
        assert this.source != null : "'source' is null";
        assert this.dest != null : "'dest' is null";

        final ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<>(100);
        final Splitter splitter = new Splitter(this.source, queue);
        final Saver saver = new Saver(this.dest, queue);

        final Thread producer = new Thread(splitter, "splitter");
        final Thread consumer = new Thread(saver, "saver");
        producer.start();
        consumer.start();
    }

    /**
     * Parses the command-line parameters provided by the user.
     * @param args The arguments provided by the user.
     */
    private void parseCommandLineParameters(String[] args) {
        assert args != null : "'args' is null";

        final CommandLineParser cliParser = new DefaultParser();
        try {
            commandLine = cliParser.parse(cliOptions, args, false);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            usage();
            System.exit(1);
        }

        if (commandLine.hasOption(HELP)) {
            usage();
            System.exit(0);
        }

        if (!commandLine.hasOption(SOURCE) || !commandLine.hasOption(DEST)) {
            System.err.println("Missing source or destination.");
            usage();
            System.exit(1);
        }

        try {
            handleVCardSource();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }

        try {
            handleVCardDest();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        }
    }

    /**
     * Checks if the vCard source file exists.
     * @throws FileNotFoundException When the source file does not exist.
     */
    private void handleVCardSource() throws FileNotFoundException {
        final String source = commandLine.getOptionValue(SOURCE);
        final File vCardSource = new File(source);
        if (!vCardSource.exists()) {
            throw new FileNotFoundException(String.format("File '%s' not found.", vCardSource));
        }
        this.source = vCardSource;
    }

    /**
     * Checks if the vCard destination is a valid location.
     * @throws FileNotFoundException When the location provided is a non-existing location or is not a directory.
     */
    private void handleVCardDest() throws FileNotFoundException {
        final String dest = commandLine.getOptionValue(DEST);
        final File vCardDest = new File(dest);
        if (!vCardDest.exists() || !vCardDest.isDirectory()) {
            throw new FileNotFoundException(String.format("File '%s' does not exist or is not a directory.", vCardDest));
        }
        this.dest = vCardDest;
    }

    /**
     * Prints usage message to stdout.
     */
    private void usage() {
        final HelpFormatter helpFormatter = new HelpFormatter();
        final String commandLineSyntax =
                "vcard-splitter [-f|--source <file> -t|--dest <dir>] [--help]\n";
        helpFormatter.printHelp(80, commandLineSyntax, "\nOPTIONS", cliOptions, "");
    }
}
