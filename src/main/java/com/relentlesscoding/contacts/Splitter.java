package com.relentlesscoding.contacts;

import java.io.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contains functionality related to splitting a vCards file into chunks.
 */
class Splitter implements Runnable {
    private final static String VCARD_BEGIN = "BEGIN:VCARD";
    private final static String VCARD_END = "END:VCARD";
    private final static Matcher whitespace = Pattern.compile("^\\p{javaWhitespace}*$").matcher("");

    private ArrayBlockingQueue<String> queue;
    private File source;

    /**
     * Constructs a new object of type {@link Splitter}.
     */
    Splitter(final File source, final ArrayBlockingQueue<String> queue) {
        if (source == null) {
            throw new NullPointerException("'source' is null");
        }
        if (queue == null) {
            throw new NullPointerException("'queue' is null");
        }

        this.queue = queue;
        this.source = source;
    }

    /**
     * Splits a file with a collection of vCards into separate
     * {@link String}s and puts them in the queue.
     */
    private void split() {
        boolean newVcard = true;

        final StringBuilder newContact = new StringBuilder();
        try (final BufferedReader reader = new BufferedReader(new FileReader(this.source))) {
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                line += "\n";
                if (!newVcard) {
                    if (line.startsWith(VCARD_BEGIN)) {
                        throw new IllegalStateException("found '${VCARD_BEGIN}' but was expecting either '${VCARD_END}' or something else");
                    }
                    if (line.startsWith(VCARD_END)) {
                        // strip newline
                        newContact.append(line.substring(0, line.length() - 1));
                        // next line should start a new contact
                        newVcard = true;
                        this.queue.put(newContact.toString());
                        // create clean slate
                        newContact.setLength(0);
                    } else {
                        newContact.append(line);
                    }
                } else {
                    if (whitespace.reset(line).matches()) {
                        // no-op: ignore whitespace
                    } else if (!line.startsWith(VCARD_BEGIN)) {
                        throw new IllegalStateException("line expected '${VCARD_BEGIN}' but was '${line}'");
                    } else {
                        // indicate that we are in the process of building a vCard
                        newVcard = false;
                        newContact.append(line);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.err.format("File does not exist: '%s'.%n", this.source);
            System.exit(2);
        } catch (IOException e) {
            System.err.format("Exception while handling file: '%s'.%n", this.source);
            System.exit(3);
        } catch (InterruptedException e) {
            System.err.println(e.getMessage());
            System.exit(4);
        }
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        synchronized (this) {
            split();
        }
    }
}
