package com.relentlesscoding.contacts;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SplitterTest {
    private final int capacity = 10;
    private ArrayBlockingQueue<String> queue;

    @Before
    public void setup() {
        queue = new ArrayBlockingQueue<>(capacity);
    }

    @After
    public void tearDown() {
        queue = null;
    }

    @Test
    public void splitShouldPutThreeElementsIntoQueue() throws URISyntaxException {
        // arrange
        final String contacts = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("contacts.vcf"))).lines().collect(Collectors.joining("\n"));

        // act
        final File file = new File(getClass().getClassLoader().getResource("contacts.vcf").toURI());
        Splitter splitter = new Splitter(file, queue);
        splitter.run();

        // assert
        assertEquals(capacity - 3, queue.remainingCapacity());
    }

    @Test
    public void allContactsShouldStartAndEndProperly() throws URISyntaxException {
        // arrange
        final String contacts = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("contacts.vcf"))).lines().collect(Collectors.joining("\n"));

        // act
        final File file = new File(getClass().getClassLoader().getResource("contacts.vcf").toURI());
        Splitter splitter = new Splitter(file, queue);
        splitter.run();

        // assert
        Object[] contactsArray = queue.toArray();
        for (Object contact : contactsArray) {
            String contactStr = (String) contact;
            assertTrue(contactStr.startsWith("BEGIN:VCARD"));
            assertTrue(contactStr.endsWith("END:VCARD"));
        }
    }
}
