package com.relentlesscoding.contacts;

import org.junit.After;
import org.junit.Before;

public class AppTest {
    private App app;

    @Before
    public void setup() {
        app = new App();
    }

    @After
    public void teardown() {
        app = null;
    }

}
