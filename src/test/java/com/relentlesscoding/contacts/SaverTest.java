package com.relentlesscoding.contacts;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SaverTest {
    private final static int capacity = 10;
    private ArrayBlockingQueue<String> queue;

    @Before
    public void setup() {
        queue = new ArrayBlockingQueue<>(capacity);
    }

    @After
    public void tearDown() {
        queue = null;
    }

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void savesContentOfThreeElemensInQueue() throws URISyntaxException, IOException, InterruptedException {
        // arrange
        final Set<String> set = new HashSet<>(3);
        set.add("abc");
        set.add("def");
        set.add("ghi");
        for (String elem : set) {
            queue.put(elem);
        }
        final File folder = tempFolder.newFolder("unit_test_temp");
        final Saver saver = new Saver(folder, queue);

        // act
        saver.run();

        // assert
        final File[] files = folder.listFiles();
        assertEquals(3, files.length);
        for (File file : files) {
            assertTrue(file.getName().endsWith(".vcf"));
            final String content = new BufferedReader(new FileReader(file)).lines().collect(Collectors.joining("\n"));
            set.contains(content);
        }
    }

    @Test
    public void saverStopsAfterASecondWhenQueueIsEmpty() throws IOException {
        // arrange
        assert queue.remainingCapacity() == capacity : "remaining capacity of queue is not equal to initial capacity";
        final File folder = tempFolder.newFolder();
        assert folder.exists() : "'folder' does not exist";
        final Saver saver = new Saver(tempFolder.newFolder(), queue);
        final LocalTime startTime = LocalTime.now();

        // act
        saver.run();
        final LocalTime endTime = LocalTime.now();

        // assert
        assertEquals(0, folder.listFiles().length);
        assertTrue(endTime.getSecond() - startTime.getSecond() >= 1);
    }
}
