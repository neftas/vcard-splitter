# vCard splitter

This program will split any version vCard file containing multiple contacts
into separate files that can then be used for importing them in applications
that expect a single `vcf` file to contain only one contact.

## How to use

```
usage: vcard-splitter [-f|--source <file> -t|--dest <dir>] [--help]

OPTIONS
 -f,--source <arg>   The vCards source file
 -h,--help           Print this help text and exit
 -t,--dest <arg>     The destination directory
```

## How to build and run

```bash
./gradlew build installDist
cd build/install/vcard-splitter/bin
./vcard-splitter --help
```

## Q: I can do the exact same thing with an `awk` oneliner (or this other fancy scripting language)!

A: Me too, although it would be a somewhat long oneliner in `awk`:

```awk
BEGIN {
    RS="END:VCARD\r?\n"
    FS="\n"
}
{
    command = "echo -n $(pwgen 20 1).vcf"
    command | getline filename
    close(command)
    print $0 "END:VCARD" > filename
}
```

## Q: So why do you put this code on the web?

A: I was experimenting with Java threads and concurrency and saw a chance to
implement a program that uses these concepts. Also, the `awk` solution above
takes more time when the numbers increase. The Java implementation is faster
at about 400 contacts - and that is mainly because it waits a full second to
see if no contacts are left to be split.
